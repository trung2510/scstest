const Colors = {
    PURPLE1: '#7C40FF',
    BLACK: '#1E2026',
    WHITE: '#FFF',
    GRAY: '#292E3C',
    TEXT_GRAY: '#595B65',
    
};

export default Colors;
