import { ColorPropType } from 'react-native';
import { Images } from '../assets/index';
import Colors from './Colors';
export const DATA = [
  { id: 0, name: 'New', isNew: true },
  { id: 1, name: 'Burgers', isNew: true },
  { id: 2, name: 'Pizza', isNew: true },
  { id: 3, name: 'Drink', isNew: true },
  { id: 4, name: 'Sushi', isNew: true },
  { id: 5, name: 'Sushi', isNew: true },
  { id: 6, name: 'Sushi', isNew: true },

];

export const thirdIndicatorStyles = {
  stepIndicatorSize: 20,
  currentStepIndicatorSize: 25,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: Colors.PURPLE1,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: Colors.PURPLE1,
  stepStrokeUnFinishedColor: Colors.TEXT_GRAY,
  separatorFinishedColor: Colors.PURPLE1,
  separatorUnFinishedColor: Colors.TEXT_GRAY,
  stepIndicatorFinishedColor: Colors.PURPLE1,
  stepIndicatorUnFinishedColor: Colors.TEXT_GRAY,
  stepIndicatorCurrentColor: Colors.WHITE,
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: 'transparent',
  stepIndicatorLabelFinishedColor: 'transparent',
  stepIndicatorLabelUnFinishedColor: 'transparent',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: Colors.WHITE,
};

export const LIST_ITEM = [
  {
    id: 1,
    name: 'Burgers',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 2,
    name: 'Pizza',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 3,
    name: 'Drink',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 4,
    name: 'Sushi',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 5,
    name: 'Sushi',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 6,
    name: 'Sushi',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
  {
    id: 7,
    name: 'Sushi',
    image: Images.humberger,
    weight: '320g',
    price: 5.24,
  },
];
