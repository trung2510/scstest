/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Colors from '../helper/Colors';

export default class AppButton extends React.Component {
  render() {
    const { style, text, numberOfLines, onPress = () => { }, styleText } = this.props;

    return (
      <TouchableOpacity {...this.props} style={style}>
        <Text
          ellipsizeMode="tail"
          numberOfLines={numberOfLines}
          allowFontScaling={false}
          style={[styles.text, styleText]}>
          {text}
          {this.props.children}
        </Text>
      </TouchableOpacity>
    );
  }
}

let styles = {
  text: {
    backgroundColor: Colors.PURPLE1,
    color: Colors.WHITE,
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderRadius: 3,
    textAlign: 'center',
  },
};
