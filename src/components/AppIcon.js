import React from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../helper/Colors';

const AppIcon = (props) => {
  let { source, resizeMode = FastImage.resizeMode.contain, style = {}, isPress = false, navigation, onPress = () => { navigation.goBack() }, isBackground = true } = props;
  return isPress ? (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <FastImage
        {...props}
        source={source}
        resizeMode={resizeMode}
        style={[styles.icon, style]}
      />
    </TouchableOpacity>
  ) :
    (
      <View style={styles.container}>
        <FastImage
          {...props}
          source={source}
          resizeMode={resizeMode}
          style={[styles.icon, style]}
        />
      </View>
    )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.TEXT_GRAY,
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 25,
    height: 25
  },
});

export default AppIcon;
