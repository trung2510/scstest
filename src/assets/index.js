const Images = {
    humberger: require('./hamburger.jpg'),
};

const ICON = {
    user: require('./ic_user.png'),
    shopping_bag: require('./shopping_bag.png'),
    close: require('./ic_close.png'),
    back: require('./back.png')
};

export { Images, ICON };
