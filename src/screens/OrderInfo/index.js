import React, { Component } from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import { View, Avatar } from 'react-native-ui-lib';
import styles from './styles';
import AppText from '../../components/AppText';
import AppIcon from '../../components/AppIcon';
import { Images, ICON } from '../../assets';
import Colors from '../../helper/Colors';
import StepIndicator from 'react-native-step-indicator';
import { DATA, LIST_ITEM } from '../../helper/Consts';
import Swiper from 'react-native-swiper';
// import { connect } from 'react-redux';
import { thirdIndicatorStyles } from '../../helper/Consts'


export default class OrderInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentPosition: 0,
        }
    }

    onStepPress(position) {
        this.setState({ currentPosition: position });
    }

    renderHeader() {
        return (
            <View row style={styles.header}>
                <AppIcon source={ICON.back} isPress navigation={this.props.navigation} />
                <AppText text="Order info" />
                <View style={{ paddingRight: 25 }} />
            </View>
        );
    }

    renderItem = (item) => {
        return (
            <View style={styles.item}>
                <Avatar containerStyle={styles.image} source={item.image} size={60} />
                <AppText style={styles.title}>{item.name}</AppText>
                <View flex />
                <AppText style={styles.weight}>{item.weight}</AppText>
                <AppText style={styles.x}> x </AppText>
                <AppText style={styles.price}>${item.price}</AppText>
            </View>
        )
    }

    renderOrderList = () => {
        return (
            <View style={styles.orderList}>
                <View row>
                    <AppText text="Order list" />
                    <View flex />
                    <AppIcon source={ICON.user} />
                </View>

                <FlatList
                    data={LIST_ITEM}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => this.renderItem(item)}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    };

    renderStatus() {
        return (
            <View style={styles.status}>
                <AppText style={{ color: Colors.TEXT_GRAY }} text="Order number" />
                <AppText style={styles.number} text="456" />
                <AppText style={{ color: Colors.TEXT_GRAY }} text="will be ready in 5 minutes" />

                <View style={{ marginVertical: 50, width: '100%' }}>
                    <StepIndicator
                        stepCount={4}
                        customStyles={thirdIndicatorStyles}
                        currentPosition={this.state.currentPosition}
                        onPress={this.onStepPress}
                    />
                </View>

                <Swiper
                    style={{ flexGrow: 1 }}
                    loop={false}
                    index={this.state.currentPosition}
                    autoplay={false}
                    onIndexChanged={page => this.onStepPress(page)}
                    showsPagination={false}
                >
                    {DATA.map((page) => {
                        return (
                            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                {this.renderOrderList()}
                            </View>
                        )
                    })}
                </Swiper>
            </View>
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.renderHeader()}
                {this.renderStatus()}
            </SafeAreaView>
        );
    }
}

// const mapStateToProps = (state) => ({});

// const mapDispatchToProps = {};

// export default connect(mapStateToProps, mapDispatchToProps)(Home);
