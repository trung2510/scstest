import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../helper/Colors';
import Color from '../../helper/Colors';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1E2026',
    flexGrow: 1,
    padding: 8
  },
  header: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingTop: '10%',
    height: 100
  },
  image: {

  },
  status: {
    alignItems: 'center',
    marginTop: '10%',
    flex: 1

  },
  orderList: {
    width: '100%',
    height: height * 0.4
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: height * 0.02
  },
  title: {
    paddingLeft: width * 0.05
  },
  number: {
    fontSize: width * 0.1
  },
  x: {
    color: Color.TEXT_GRAY
  },




  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }

});

export default styles;
