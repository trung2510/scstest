import React, { Component } from 'react';
import { FlatList, SafeAreaView, TouchableOpacity } from 'react-native';
import { View, Avatar } from 'react-native-ui-lib';
import styles from './styles';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { LIST_ITEM, DATA } from '../../helper/Consts';
import AppText from '../../components/AppText';
import AppIcon from '../../components/AppIcon';
import Colors from '../../helper/Colors';
import { Images, ICON } from '../../assets';
import AppButton from '../../components/AppButton';
// import { connect } from 'react-redux';
// var ScrollableTabView = require('react-native-scrollable-tab-view');


export default class Home extends Component {

  renderHeader() {
    return (
      <View row style={styles.header}>
        <AppIcon source={ICON.user} />
        <AppText />
        <AppIcon isPress navigation={this.props.navigation} source={ICON.shopping_bag} />
      </View>
    );
  }

  renderLocation() {
    return (
      <View row paddingH-8 paddingV-20 centerV >
        <View>
          <AppText style={styles.take}>Take away from</AppText>
          <AppText>71 Tottenham Court Rd</AppText>
        </View>
        <AppText style={{ flex: 1 }} />
        <AppIcon source={ICON.user} />
      </View>
    );
  }

  //Item flatlist
  renderItem = ({ item }) => {
    const { navigation } = this.props;
    return (
      <TouchableOpacity onPress={() => { navigation.navigate('ProductDetail', { item: item }) }} style={styles.item}>
        <Avatar containerStyle={styles.image} source={item.image} size={70} />
        <View style={styles.itemInfo}>
          <AppText style={styles.title}>{item.name}</AppText>
          <AppText style={styles.weight}>{item.weight}</AppText>
          <AppText style={styles.price}>{item.price}</AppText>
        </View>
      </TouchableOpacity>
    );
  };

  //List Product
  renderListPage = (e, i) => {
    return (
      <View row style={styles.vList} tabLabel={e.name} key={i.toString()}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={LIST_ITEM}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
    );
  };

  //Scroll Tabview
  renderList() {
    return (
      <ScrollableTabView
        tabBarActiveTextColor={Colors.WHITE}
        tabBarInactiveTextColor={Colors.TEXT_GRAY}
        tabBarUnderlineStyle={{ height: 3, backgroundColor: Colors.PURPLE1 }}

      >
        {DATA.map((e, i) => this.renderListPage(e, i))}
      </ScrollableTabView>
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.renderHeader()}
        {this.renderLocation()}
        {this.renderList()}
      </SafeAreaView>
    );
  }
}

// const mapStateToProps = (state) => ({});

// const mapDispatchToProps = {};

// export default connect(mapStateToProps, mapDispatchToProps)(Home);
