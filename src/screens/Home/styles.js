import { Dimensions, StyleSheet } from 'react-native';
import Color from '../../helper/Colors';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1E2026',
    flex: 1,
  },
  header: {
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingTop: '10%',
  },
  vList: {
    paddingHorizontal: 8,
    paddingTop: '5%',
  },
  item: {
    backgroundColor: Color.GRAY,
    paddingHorizontal: 8,
    flexDirection: 'row',
    marginVertical: 6,
    borderRadius: 10,
    paddingVertical: 15,
    alignItems:'center'
  },
  itemInfo: {
    paddingVertical: 5,
  },
  image: {
    marginRight: 16,
    marginLeft: 8,
  },
  price: {
    backgroundColor: Color.PURPLE1,
    color: Color.WHITE,
    paddingHorizontal: 20,
    paddingVertical: 4,
    borderRadius: 3,
    marginTop: 10
  },
  weight: {
    color: Color.TEXT_GRAY
  },
  take: {
    color: Color.TEXT_GRAY
  }


});

export default styles;
