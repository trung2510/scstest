import React, { Component } from 'react';
import { View, TextField, Text, Button } from 'react-native-ui-lib';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onLogin = () => {
    const { navigation } = this.props;
    navigation.navigate('Home');
  }

  render() {
    return (
      <View flex paddingT-120 paddingH-25>
        <Text blue50 text20>
          {' '}
          Welcome{' '}
        </Text>
        <TextField text50 placeholder="username" dark10 />
        <TextField text50 placeholder="password" secureTextEntry dark10 />
        <View marginT-100 center>
          <Button
            text70
            white
            background-orange30
            label="Login"
            onPress={this.onLogin}
          />
          <Button link text70 orange30 label="Sign Up" marginT-20 />
        </View>
      </View>
    );
  }
}
