import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { View } from 'react-native-ui-lib';
import styles from './styles';
import FastImage from 'react-native-fast-image';
import AppText from '../../components/AppText';
import AppIcon from '../../components/AppIcon';
import { Images, ICON } from '../../assets';
import AppButton from '../../components/AppButton';
import Colors from '../../helper/Colors';
// import { connect } from 'react-redux';
// var ScrollableTabView = require('react-native-scrollable-tab-view');

export default class ProductDetail extends Component {

    renderTopView = () => {
        const item = this.props;
        const { image } = item.route.params.item;
        console.log(item.route.params.item);

        return (
            <View style={styles.topView}>
                <View style={styles.box}>
                    <AppIcon source={ICON.close} isPress navigation={this.props.navigation} />
                </View>
                <FastImage source={image} style={styles.image} />
            </View>
        );
    };

    renderContent = () => {
        const item = this.props;
        const { price, weight } = item.route.params.item;
        return (
            <View style={styles.contentView}>
                <AppButton style={styles.button} text="New" />
                <AppText style={styles.title} text="Big double cheeseburger" />
                <AppText
                    style={styles.text}
                    text="Marble beef, cheddar cheese, jalapeno peper, pickled cucumber, lettuce, red onion, BBQ sauce"
                />
                <View row center marginT-8>
                    <View row centerV>
                        <FastImage source={ICON.user} style={styles.icon} />
                        <AppText text={'$' + price} />
                    </View>

                    <View row centerV paddingH-8>
                        <FastImage source={ICON.user} style={styles.icon} />
                        <AppText text={weight} />
                    </View>
                </View>
            </View>
        );
    };

    renderBotBut = () => {
        const { navigation, route } = this.props;
        const { price } = route.params.item;
        return (
            <AppButton
                onPress={() => { navigation.navigate('OrderInfo') }}
                style={styles.btnTaste}
                styleText={styles.txtBtn}
                text={'Taste it for $' + price} />
        );
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.renderTopView()}
                {this.renderContent()}
                {this.renderBotBut()}
            </SafeAreaView>
        );
    }
}

// const mapStateToProps = (state) => ({});

// const mapDispatchToProps = {};

// export default connect(mapStateToProps, mapDispatchToProps)(Home);
