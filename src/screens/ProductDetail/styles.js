import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../helper/Colors';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1E2026',
    flexGrow: 1,
    padding: 8
  },
  image: {
    width: '100%',
    height: height * 0.3,
    resizeMode: 'stretch',
    marginTop: '10%',
  },
  topView: {
    flex: 4,
    alignItems: 'center',
    paddingTop: '10%',
  },
  box: {
    alignItems: 'flex-end',
    width: '100%',
  },
  contentView: {
    flex: 5,
    marginTop: '15%',
    alignItems: 'center',
  },
  text: {
    paddingHorizontal: '5%',
    fontSize: 15,
    textAlign: 'center',
    color: Colors.TEXT_GRAY,
    marginTop: '3%'

  },
  button: {
    marginTop: '5%'
  },

  title: {
    fontSize: 35,
    textAlign: 'center',
    marginTop: '3%'
  },
  btnTaste: {
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    width: width * 0.9,

  },
  txtBtn: {
    paddingVertical: 12,
    borderRadius: 12,
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 10
  },

});

export default styles;
